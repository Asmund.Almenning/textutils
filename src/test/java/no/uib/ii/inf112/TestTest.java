package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = (width - text.length());
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = (width - text.length());
			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			String words[] = words(text);
			int extra = (text.length() - width + words.length -1) / (words.length -1);
			
			String returnText = "";
			for (String word : words) {
				returnText = returnText + word + " ".repeat(extra);
			}
			return returnText;
		}
		
		public String[] words(String text) {
			String words[] = text.split("\\s");
			return words;
		}
	};
		

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
	}
	
	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("  foo", aligner.flushRight("foo", 5));
	}
	
	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
	}
	
	@Test
	void testJustify() {
		assertEquals("a  b  c", aligner.justify("a b c", 7));
	}
}
